cmake_minimum_required(VERSION 3.15)
project(collision)

set(CMAKE_CXX_STANDARD 14)

add_executable(collision
        src/main.cpp
        include/parser.h
        src/parser.cpp
        include/program.h
        include/program_impl.h
        src/program.cpp
        include/collision.h
        src/collision.cpp
        include/shape.h
        src/shape.cpp
        # engine + glad
        engine/glad/include/KHR/khrplatform.h
        engine/glad/include/glad.h
        engine/glad/src/glad.c
        engine/include/engine.h
        engine/src/engine.cpp)

target_include_directories(collision
        PRIVATE
        include
        engine/include
        engine/glad/include)

find_package(SDL2 REQUIRED)
target_link_libraries(collision
        PRIVATE
        SDL2::SDL2
        SDL2::SDL2main)