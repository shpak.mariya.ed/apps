# Collision based SAT algorithm

## About
The program based SDL2 + OpenGL collision visualization. Collision is detected by SAT algorithm.

## Screenshots
![collision_0](screenshots/collision_0.png)

## How to use 
If collision exists, collided shapes become red (except the selected one).
Program supports runtime shapes changing (by shapes.txt file).
Control:
- **space** to update shapes by file config
- **tab** to choose the next shape (selected shape is marked as blue)
- **arrows** or **wasd** to move selected shape
- **esc** to close window

## How to get
If you use IDE, select as a working directory **collision** folder.
Using terminal (from repo root):

    cd collision/
    cmake -S . -B collision/build/
    cmake --build build/
    ./build/coliision 
   
## Dependencies
- GCC 6.0* (with C++ 14 features)
- CMake 3.15.0
- SDL2 2.0.0