#pragma once

#include <SDL2/SDL_video.h>

#include "shape.h"

namespace engine {
enum keys { esc, tab, space, up, down, left, right, quit };

struct vec3 {
    float x = 0.f;
    float y = 0.f;
    float z = 0.f;
};

class engine {
    SDL_GLContext context;
    SDL_Window *window = nullptr;
    unsigned int program_id = 0;

public:
    ~engine();
    void init();
    void terminate();
    bool read_event(keys &k);
    void swap_buffers() const;
    void set_uniform(const std::string &s, const vec3 &v3) const;
    void draw_shape(const om::shape *s, bool selected = false) const;
};
} // namespace engine