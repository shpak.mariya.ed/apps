#include <cmath>
#include <stdexcept>
#include <string>
#include <vector>

#include "engine.h"
#include "glad.h"

#include <SDL2/SDL.h>

namespace engine {
engine::~engine() {}
void engine::init() {
    // sdl init
    const int init_result = SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS);
    if (init_result != 0) {
        throw std::runtime_error(std::string("Error SDL init: ") +
                                 SDL_GetError());
    }
    // setting context's attributes
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                        SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    // window creation
    window =
        SDL_CreateWindow("Collision program", SDL_WINDOWPOS_CENTERED,
                         SDL_WINDOWPOS_CENTERED, 720, 720, SDL_WINDOW_OPENGL);
    if (window == nullptr) {
        throw std::runtime_error(std::string("Error window creation: ") +
                                 SDL_GetError());
    }
    // context creation
    context = SDL_GL_CreateContext(window);
    if (context == nullptr) {
        throw std::runtime_error(std::string("Error context creation: ") +
                                 SDL_GetError());
    }
    // glad loading
    if (gladLoadGLES2Loader(SDL_GL_GetProcAddress) == 0) {
        throw std::runtime_error("Failed to initialize GLAD.");
    }
    // shaders creation
    // vertex shader
    GLuint vertex_shader_id = glCreateShader(GL_VERTEX_SHADER);
    if (vertex_shader_id == 0) {
        throw std::runtime_error(
            "Cannot create vertex shader! glCreateShader returned 0.");
    }
    const std::string vertex_shader_src = R"(#version 330 core
layout (location = 0) in vec2 a_pos;
void main() {
    gl_Position = vec4(a_pos, 0.0, 1.0);
})";
    const char *shader_source = vertex_shader_src.data();
    glShaderSource(vertex_shader_id, 1, &shader_source, nullptr);
    glCompileShader(vertex_shader_id);
    GLint compiled_status = 0;
    glGetShaderiv(vertex_shader_id, GL_COMPILE_STATUS, &compiled_status);
    if (compiled_status == GL_FALSE) {
        throw std::runtime_error("Error compiling vertex shader.");
    }
    // fragment shader
    GLuint fragment_shader_id = glCreateShader(GL_FRAGMENT_SHADER);
    if (fragment_shader_id == 0) {
        throw std::runtime_error(
            "Cannot create fragment shader! glCreateShader returned 0.");
    }
    const std::string fragment_shader_src = R"(#version 330 core
uniform vec3 color;
void main() {
    gl_FragColor = vec4(color, 1.0);
})";
    shader_source = fragment_shader_src.data();
    glShaderSource(fragment_shader_id, 1, &shader_source, nullptr);
    glCompileShader(fragment_shader_id);
    compiled_status = 0;
    glGetShaderiv(fragment_shader_id, GL_COMPILE_STATUS, &compiled_status);
    if (compiled_status == GL_FALSE) {
        throw std::runtime_error("Error compiling fragment shader.");
    }
    // program creation
    program_id = glCreateProgram();
    if (program_id == 0) {
        throw std::runtime_error("Error gl program creation.");
    }
    // shaders attach
    glAttachShader(program_id, vertex_shader_id);
    glAttachShader(program_id, fragment_shader_id);
    glLinkProgram(program_id);
    GLint linked_status = 0;
    glGetProgramiv(program_id, GL_LINK_STATUS, &linked_status);
    if (linked_status == GL_FALSE) {
        throw std::runtime_error("Error linking gl program.");
    }
    glUseProgram(program_id);
    // vbo, vao
    GLuint vbo = 0, vao = 0;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
}
void engine::terminate() {
    SDL_GL_DeleteContext(context);
    SDL_DestroyWindow(window);
    SDL_Quit();
}
bool engine::read_event(keys &k) {
    SDL_Event sdl_event;
    while (SDL_PollEvent(&sdl_event) > 0) {
        if (sdl_event.type == SDL_QUIT) {
            k = keys::quit;
            return true;
        } else if (sdl_event.type == SDL_KEYDOWN) {
            SDL_Keycode key_code = sdl_event.key.keysym.sym;
            switch (key_code) {
            case SDLK_ESCAPE:
                k = keys::esc;
                return true;
            case SDLK_TAB:
                k = keys::tab;
                return true;
            case SDLK_SPACE:
                k = keys::space;
                return true;
            case SDLK_UP:
            case SDLK_w:
                k = keys::up;
                return true;
            case SDLK_DOWN:
            case SDLK_s:
                k = keys::down;
                return true;
            case SDLK_LEFT:
            case SDLK_a:
                k = keys::left;
                return true;
            case SDLK_RIGHT:
            case SDLK_d:
                k = keys::right;
                return true;
            default:
                return false;
            }
        }
    }
    return false;
}
void engine::swap_buffers() const {
    SDL_GL_SwapWindow(window);
    glClearColor(1.f, 1.f, 1.f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
}
void engine::set_uniform(const std::string &s, const vec3 &v3) const {
    GLint uniform_location = glGetUniformLocation(program_id, s.data());
    glUniform3f(uniform_location, v3.x, v3.y, v3.z);
}
static void get_circle_dots(const om::circle *c,
                            std::vector<om::vertex> &vertices) {
    om::vertex center = c->get_center();
    float r = c->get_radius();
    float rad_ = 180.f / static_cast<float>(M_PI);
    for (int i = 0; i <= 360; ++i) {
        float rad = static_cast<float>(i) / rad_;
        float x = r * cos(rad) + center.x;
        float y = r * sin(rad) + center.y;
        vertices.push_back({x, y});
    }
}
void engine::draw_shape(const om::shape *s, bool is_selected) const {
    if (s == nullptr) return;
    // setting color - red if collision, blue is selected
    vec3 color;
    if (is_selected) {
        color = {0.f, 0.f, 1.f};
    } else if (s->mark()) {
        color = {1.f, 0.f, 0.f};
    } else {
        color = {0.5f, 0.5f, 0.5f};
    }
    set_uniform("color", color);
    // getting shape vertices
    std::vector<om::vertex> vertices;
    if (s->get_type() == om::shapes::circle) {
        const om::circle *c = dynamic_cast<const om::circle *>(s);
        // todo
        get_circle_dots(c, vertices);
    } else {
        const om::polygon *p = dynamic_cast<const om::polygon *>(s);
        vertices = p->get_vertices();
    }
    // drawing
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(om::vertex),
                 &vertices.front(), GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float),
                          (void *)0);
    glDrawArrays(GL_LINE_LOOP, 0, vertices.size());
    glDisableVertexAttribArray(0);
}
} // namespace engine