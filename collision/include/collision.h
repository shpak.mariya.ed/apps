#pragma once

#include "shape.h"

namespace om {
bool is_collision(shape *s1, shape *s2);
}