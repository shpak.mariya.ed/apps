#include <string>
#include <unordered_map>
#include <vector>

namespace om {
using config_map_t = std::unordered_map<std::string, std::string>;

float str_to_float(const std::string &s);
std::string trim(const std::string &s);
std::vector<std::string> get_split_string(const std::string &s,
                                          const char del = ':');
config_map_t get_config_map_from_string(const std::string &s,
                                        const char major_del = '\n',
                                        const char minor_del = ':');
config_map_t get_config_map_from_file(const std::string &path,
                                      const char major_del = '\n',
                                      const char minor_del = ':');
std::vector<config_map_t>
get_list_of_config_map_from_file(const std::string &path,
                                 const char major_del = '\n',
                                 const char minor_del = ':');
} // namespace om