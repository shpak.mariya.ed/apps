#pragma once

#include <string>

namespace om {
class program {
public:
    virtual ~program();
    virtual void input_file(const std::string &path) = 0;
    virtual void start() = 0;
};

program *create_program();
void destroy_program(program *p);
} // namespace om