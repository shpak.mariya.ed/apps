#pragma once

#include <memory>
#include <vector>
#include <string>

#include "engine.h"
#include "program.h"
#include "shape.h"

namespace om {
class program_impl : public program {
    bool is_running = true;
    std::string input_path;
    engine::engine e;
    std::vector<std::unique_ptr<shape>> shapes;
    shape *selected_shape = nullptr;

    void select_next_shape();
    void check_collisions();

public:
    program_impl();
    ~program_impl() override;
    void input_file(const std::string &path) override;
    void start() override;
    // loop methods
    void read_events();
    void update();
    void render();
};
}