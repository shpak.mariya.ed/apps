#pragma once

#include <ostream>
#include <vector>

namespace om {
enum class shapes { circle, polygon };
enum directions { up, down, left, right };

struct vect {
    float x = 0.f;
    float y = 0.f;
};

struct vertex {
    float x = 0.f;
    float y = 0.f;
};

class shape {
    bool is_on_collision = false;

public:
    virtual ~shape();
    virtual shapes get_type() const = 0;
    virtual void move(directions dir) = 0;
    virtual bool get_min_max_dots(const vect &vec, float &min, float &max) const = 0;
    // checking for collision mark
    void mark(bool m);
    bool mark() const;
};

class polygon : public shape {
    std::vector<vertex> vertices;

public:
    ~polygon() override;
    shapes get_type() const override;
    void move(directions dir) override;
    void update(const std::vector<vertex> &v);
    std::vector<vect> get_normals() const;
    const std::vector<vertex> &get_vertices() const;
    bool get_min_max_dots(const vect &vec, float &min, float &max) const override;
    vertex get_closest_dot(const vertex &v) const;
};

class circle : public shape {
    vertex center;
    float radius = 0.f;

public:
    ~circle() override;
    shapes get_type() const override;
    void move(directions dir) override;
    void update(const vertex &c, const float r);
    const vertex &get_center() const;
    float get_radius() const;
    bool get_min_max_dots(const vect &vec, float &min, float &max) const override;
    vect get_axis_from_center(const vertex &v);
};

// operators
std::vector<vect> &operator+=(std::vector<vect> &l, const std::vector<vect> &r);
std::ostream &operator<<(std::ostream &os, const vertex &v);
std::ostream &operator<<(std::ostream &os, const circle &c);
std::ostream &operator<<(std::ostream &os, const polygon &p);
} // namespace om