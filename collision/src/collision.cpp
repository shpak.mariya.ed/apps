#include <iostream>
#include <vector>

#include "collision.h"

namespace om {
// For basic polygon + polygon:
// 1. Get each shape normals.
// 2. Get each shape min max vertices (base each normal).
// 3. Check for overlap!

// For basic polygon + circle:
// 1. Get polygons normals.
// 2. Get normal from circle center and the polygon's closest dot.
// 3. Get each shape min max vertices (base each normal).
// 4. Check if center is inside of polygon.
// 5. Check for overlap!

// For basic circle + circle:
// Just use math formula of circle collision detection.

// circle + circle
static bool is_circles_collision(const circle *c1, const circle *c2) {
    float dx = c2->get_center().x - c1->get_center().x;
    float dy = c2->get_center().y - c1->get_center().y;
    float rad = c1->get_radius() + c2->get_radius();
    return ((dx * dx) + (dy * dy) < (rad * rad));
}
// polygon + polygon / polygon + circle
static bool is_shapes_collision(const shape *s1, const shape *s2,
                                const std::vector<vect> &normals) {
    for (const vect &n : normals) {
        float min1, max1, min2, max2;
        s1->get_min_max_dots(n, min1, max1);
        s2->get_min_max_dots(n, min2, max2);
        // check overlap
        bool is_divided = (min1 > max2 || min2 > max1);
        if (is_divided) return false;
    }
    return true;
}

bool is_collision(shape *s1, shape *s2) {
    if (s1 == nullptr || s2 == nullptr) return false;
    shapes t1 = s1->get_type();
    shapes t2 = s2->get_type();
    if (t1 == shapes::circle && t2 == shapes::circle) {
        circle *c1 = dynamic_cast<circle *>(s1);
        circle *c2 = dynamic_cast<circle *>(s2);
        if (is_circles_collision(c1, c2)) {
            std::cout << "Collision has been detected between: " << *c1
                      << " and " << *c2 << std::endl;
            return true;
        }
    } else {
        // getting normals
        if (t1 == shapes::polygon && t2 == shapes::polygon) {
            polygon *p1 = dynamic_cast<polygon *>(s1);
            std::vector<vect> normals = p1->get_normals();
            polygon *p2 = dynamic_cast<polygon *>(s2);
            normals += p2->get_normals();
            if (is_shapes_collision(s1, s2, normals)) {
                std::cout << "Collision has been detected between: " << *p1
                          << " and " << *p2 << std::endl;
                return true;
            }
        } else if (t1 == shapes::polygon && t2 == shapes::circle) {
            polygon *p = dynamic_cast<polygon *>(s1);
            std::vector<vect> normals = p->get_normals();
            circle *c = dynamic_cast<circle *>(s2);
            normals.push_back(
                c->get_axis_from_center(p->get_closest_dot(c->get_center())));
            if (is_shapes_collision(s1, s2, normals)) {
                std::cout << "Collision has been detected between: " << *p
                          << " and " << *c << std::endl;
                return true;
            }
        } else {
            polygon *p = dynamic_cast<polygon *>(s2);
            std::vector<vect> normals = p->get_normals();
            circle *c = dynamic_cast<circle *>(s1);
            normals.push_back(
                c->get_axis_from_center(p->get_closest_dot(c->get_center())));
            if (is_shapes_collision(s1, s2, normals)) {
                std::cout << "Collision has been detected between: " << *p
                          << " and " << *c << std::endl;
                return true;
            }
        }
    }
    return false;
}
} // namespace om