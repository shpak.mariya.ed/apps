#include <memory>
#include <string>

#include "program.h"

int main() {
    std::unique_ptr<om::program, void (*)(om::program *)> program(
        om::create_program(), om::destroy_program);
    const std::string path = "shapes.txt";

    program->input_file(path);
    program->start();

    return 0;
}