#include <algorithm>
#include <iostream>
#include <memory>
#include <stdexcept>

#include "collision.h"
#include "parser.h"
#include "program_impl.h"
#include "shape.h"

namespace om {
program::~program() {}

bool already_exist = false;
program *create_program() {
    if (!already_exist) {
        already_exist = true;
        program *p = new program_impl();
        return p;
    } else {
        throw std::runtime_error("Program's been already created.");
    }
}
void destroy_program(program *p) {
    already_exist = false;
    delete p;
}

program_impl::program_impl() { e.init(); }
program_impl::~program_impl() { e.terminate(); }
void program_impl::input_file(const std::string &path) { input_path = path; }
void program_impl::start() {
    if (input_path.empty()) {
        std::cout << "Input file path hasn't been set up.\n"
                     "Use input_file(path) before start().\n";
        return;
    }
    // reading input
    update();
    // program loop
    while (is_running) {
        read_events();
        render();
    }
}
void program_impl::read_events() {
    engine::keys k;
    if (e.read_event(k)) {
        switch (k) {
        case engine::keys::esc:
        case engine::keys::quit:
            is_running = false;
            break;
        case engine::keys::space:
            update();
            break;
        case engine::keys::left:
            selected_shape->move(directions::left);
            check_collisions();
            break;
        case engine::keys::right:
            selected_shape->move(directions::right);
            check_collisions();
            break;
        case engine::keys::up:
            selected_shape->move(directions::up);
            check_collisions();
            break;
        case engine::keys::down:
            selected_shape->move(directions::down);
            check_collisions();
            break;
        case engine::keys::tab:
            select_next_shape();
            break;
        }
    }
}
vertex get_vertex_by_string(const std::string &s) {
    vertex out;
    if (!s.empty()) {
        size_t del_pos = s.find(',');
        std::string x_str = s.substr(1, del_pos - 1);
        std::string y_str = s.substr(del_pos + 1, s.size() - del_pos - 2);
        out.x = str_to_float(x_str);
        out.y = str_to_float(y_str);
    }

    return out;
}
void program_impl::update() {
    // clear current shapes list
    shapes.clear();
    // getting shapes from the input file
    std::vector<config_map_t> shapes_map =
        om::get_list_of_config_map_from_file(input_path);
    for (config_map_t &s : shapes_map) {
        if (s.count("type") > 0) {
            const std::string &type = s.at("type");
            if (type == "circle") {
                circle c;
                vertex center = get_vertex_by_string(s["center"]);
                float radius = str_to_float(s["radius"]);
                c.update(center, radius);
                shapes.push_back(std::make_unique<circle>(c));
            } else if (type == "polygon") {
                polygon p;
                std::vector<vertex> vertices;
                std::vector<std::string> vert_strings =
                    get_split_string(s["vertices"], ' ');
                std::transform(vert_strings.begin(), vert_strings.end(),
                               std::back_inserter(vertices),
                               [](const std::string &v) {
                                   return get_vertex_by_string(v);
                               });
                p.update(vertices);
                shapes.push_back(std::make_unique<polygon>(p));
            }
        }
    }
    select_next_shape();
    check_collisions();
}
void program_impl::render() {
    for (auto &s : shapes) {
        if (s.get() == selected_shape) e.draw_shape(s.get(), true);
        else
            e.draw_shape(s.get());
    }
    e.swap_buffers();
}
void program_impl::select_next_shape() {
    if (shapes.empty()) return;
    if (selected_shape == nullptr) {
        selected_shape = shapes.at(0).get();
    } else {
        for (size_t i = 0; i < shapes.size(); ++i) {
            if (shapes.at(i).get() == selected_shape) {
                if (i == shapes.size() - 1) {
                    selected_shape = shapes.at(0).get();
                } else {
                    selected_shape = shapes.at(i + 1).get();
                }
                break;
            }
        }
    }
}
void program_impl::check_collisions() {
    for (auto &s : shapes) {
        s.get()->mark(false);
    }
    bool is_any_collision_detect = false;
    for (size_t i = 0; i < shapes.size(); ++i) {
        shape *s = shapes.at(i).get();
        for (size_t j = i + 1; j < shapes.size(); ++j) {
            shape *next = shapes.at(j).get();
            if (is_collision(s, next)) {
                is_any_collision_detect = true;
                s->mark(true);
                next->mark(true);
            }
        }
    }
    if (is_any_collision_detect) std::cout << std::endl;
}
} // namespace om