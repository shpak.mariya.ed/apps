# Collision based SAT algorithm

## About
Using SAT algorithm. The visualization you can find [here](https://gitlab.com/shpak.mariya.ed/apps/-/tree/master/collision) :)

## How to get
If you use IDE, select as a working directory **collision** folder.
Using terminal (from repo root):

    cd collision/
    cmake -S . -B collision/build/
    cmake --build build/
    ./build/coliision 
   
## Dependencies
- GCC 6.0* (with C++ 14 features)
- CMake 3.15.0