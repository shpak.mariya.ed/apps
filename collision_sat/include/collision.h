#pragma once

#include "shape.h"

bool is_collision(shape *s1, shape *s2);