#include <algorithm>
#include <memory>
#include <string>
#include <vector>

#include "collision.h"
#include "parser.h"
#include "shape.h"

using shapes_vector = std::vector<std::unique_ptr<shape>>;

void parse_file(shapes_vector &shapes, const std::string &path);
void check_collision(const shapes_vector &shapes, shape *s);

int main() {
    shapes_vector shapes;
    const std::string path = "shapes.txt";
    parse_file(shapes, path);
    return 0;
}

vertex get_vertex_by_string(const std::string &s) {
    vertex out;
    if (!s.empty()) {
        size_t del_pos = s.find(',');
        std::string x_str = s.substr(1, del_pos - 1);
        std::string y_str = s.substr(del_pos + 1, s.size() - del_pos - 2);
        out.x = str_to_float(x_str);
        out.y = str_to_float(y_str);
    }

    return out;
}
void parse_file(shapes_vector &shapes, const std::string &path) {
    std::vector<config_map_t> shapes_map =
        get_list_of_config_map_from_file(path);
    for (config_map_t &s : shapes_map) {
        if (s.count("type") > 0) {
            const std::string &type = s.at("type");
            if (type == "circle") {
                circle c;
                vertex center = get_vertex_by_string(s["center"]);
                float radius = str_to_float(s["radius"]);
                c.update(center, radius);
                shapes.push_back(std::make_unique<circle>(c));
                check_collision(shapes, shapes.back().get());
            } else if (type == "polygon") {
                polygon p;
                std::vector<vertex> vertices;
                std::vector<std::string> vert_strings =
                    get_split_string(s["vertices"], ' ');
                std::transform(vert_strings.begin(), vert_strings.end(),
                               std::back_inserter(vertices),
                               [](const std::string &v) {
                                 return get_vertex_by_string(v);
                               });
                p.update(vertices);
                shapes.push_back(std::make_unique<polygon>(p));
                check_collision(shapes, shapes.back().get());
            }
        }
    }
}
void check_collision( const shapes_vector &shapes, shape *s) {
    for (auto const& i: shapes) {
        if (s != i.get()) is_collision(s, i.get());
    }
}