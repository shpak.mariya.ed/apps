#include <fstream>
#include <iostream>
#include <sstream>

#include "parser.h"

float str_to_float(const std::string &s) {
    float res;
    try {
        res = std::stof(s);
    } catch (std::invalid_argument) {
        std::cerr << "Could not convert " << s << " into float!" << std::endl;
    }

    return res;
}
std::string trim(const std::string &s) {
    size_t first = s.find_first_not_of(' ');
    if (first == std::string::npos) return s;
    size_t last = s.find_last_not_of(' ');
    return s.substr(first, (last - first + 1));
}
std::vector<std::string> get_split_string(const std::string &s,
                                          const char del) {
    using namespace std;
    vector<string> out;
    if (s.empty()) {
        cerr << "Line for split is empty!" << endl;
        return out;
    }

    istringstream ss(s);
    string line;
    while (getline(ss, line, del) || getline(ss, line)) {
        if (!line.empty()) {
            out.push_back(trim(line));
        }
    }

    return out;
}
config_map_t get_config_map_from_file(const std::string &path,
                                      const char major_del,
                                      const char minor_del) {
    using namespace std;
    config_map_t out;
    if (path.empty()) {
        cerr << "Path is empty!" << endl;
        return out;
    }

    ifstream file(path);
    if (!file) throw runtime_error("Error opening file : " + path);
    string line;
    while (getline(file, line, major_del)) {
        if (!line.empty()) {
            vector<string> splitted = get_split_string(line, minor_del);
            out[splitted.at(0)] = splitted.at(1);
        }
    }

    return out;
}
config_map_t get_config_map_from_string(const std::string &s,
                                        const char major_del,
                                        const char minor_del) {
    using namespace std;
    config_map_t out;
    if (s.empty()) {
        cerr << "String is empty!" << endl;
        return out;
    }

    istringstream file(s);
    string line;
    while (getline(file, line, major_del)) {
        if (!line.empty()) {
            vector<string> splitted = get_split_string(line, minor_del);
            out[splitted.at(0)] = splitted.at(1);
        }
    }

    return out;
}
std::vector<config_map_t>
get_list_of_config_map_from_file(const std::string &path, const char major_del,
                                 const char minor_del) {
    // if major_del is '\n' global objects delimiter is empty line
    // else global delimiter is '\n'
    using namespace std;
    std::vector<config_map_t> out;
    if (path.empty()) {
        cerr << "Path is empty!" << endl;
        return out;
    }

    ifstream file(path);
    if (!file) throw runtime_error("Error opening file : " + path);
    string config;
    if (major_del == '\n') {
        string line;
        while (getline(file, line)) {
            if (!line.empty()) {
                config += line + '\n';
            } else {
                out.push_back(
                    get_config_map_from_string(config, major_del, minor_del));
                config.clear();
            }
        }
        if (!config.empty()) {
            out.push_back(
                get_config_map_from_string(config, major_del, minor_del));
        }
    } else {
        while (getline(file, config)) {
            out.push_back(
                get_config_map_from_string(config, major_del, minor_del));
        }
    }

    return out;
}