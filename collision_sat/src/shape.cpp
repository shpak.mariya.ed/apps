#include <cmath>

#include "shape.h"

static vect get_axis(const vertex &v1, const vertex &v2);
static vect get_normal(const vertex &v1, const vertex &v2);
static float get_dot(const vect &vec, const vertex &vert);
static vect normalize(const vect &vec);
// shape
shape::~shape() {}
// polygon
polygon::~polygon() {}
shapes polygon::get_type() const { return shapes::polygon; }
void polygon::update(const std::vector<vertex> &v) { vertices = v; }
std::vector<vect> polygon::get_normals() const {
    std::vector<vect> out;
    if (vertices.size() == 0) return out;

    for (size_t i = 1; i < vertices.size(); ++i) {
        const vertex v1 = vertices.at(i - 1);
        const vertex v2 = vertices.at(i);
        out.push_back(get_normal(v1, v2));
    }

    const vertex v1 = vertices.at(vertices.size() - 1);
    const vertex v2 = vertices.at(0);
    out.push_back(get_normal(v1, v2));

    return out;
}
const std::vector<vertex> &polygon::get_vertices() const { return vertices; }
bool polygon::get_min_max_dots(const vect &vec, float &min, float &max) const {
    if (vertices.size() < 3) return false;
    min = get_dot(vec, vertices.at(0));
    max = min;
    for (const vertex &v : vertices) {
        float temp = get_dot(vec, v);
        if (temp < min) min = temp;
        if (temp > max) max = temp;
    }

    return true;
}
static float get_dist(const vertex &v1, const vertex &v2) {
    return sqrt((v1.x - v2.x) * (v1.x - v2.x) + (v1.y - v2.y) * (v1.y - v2.y));
}
vertex polygon::get_closest_dot(const vertex &vert) const {
    if (vertices.empty()) return {};
    vertex closest_vert = vertices.at(0);
    float closest_dist = get_dist(closest_vert, vert);
    for (const vertex &v : vertices) {
        float dist = get_dist(v, vert);
        if (dist < closest_dist) {
            closest_vert = v;
            closest_dist = dist;
        }
    }

    return closest_vert;
}
// circle
circle::~circle() {}
shapes circle::get_type() const { return shapes::circle; }
void circle::update(const vertex &c, const float r) {
    center = c;
    radius = r;
}
const vertex &circle::get_center() const { return center; }
float circle::get_radius() const { return radius; }
bool circle::get_min_max_dots(const vect &vec, float &min, float &max) const {
    float center_project = get_dot(vec, center);
    min = center_project - radius;
    max = center_project + radius;
    return true;
}
vect circle::get_axis_from_center(const vertex &v) {
    return get_axis(v, center);
}

// operators
std::vector<vect> &operator+=(std::vector<vect> &v1,
                              const std::vector<vect> &v2) {
    v1.insert(v1.end(), v2.begin(), v2.end());
    return v1;
}
std::ostream &operator<<(std::ostream &os, const vertex &v) {
    os << "{" << v.x << ", " << v.y << "}";
    return os;
}
std::ostream &operator<<(std::ostream &os, const circle &c) {
    const vertex &center = c.get_center();
    os << "Circle (center: " << c.get_center() << ", radius: " << c.get_radius()
       << ")";
    return os;
}
std::ostream &operator<<(std::ostream &os, const polygon &p) {
    const std::vector<vertex> &vertices = p.get_vertices();
    os << "Polygon (vertices: " << vertices.size() << ", {";
    for (size_t i = 0; i < vertices.size(); ++i) {
        if (i != 0) os << ", ";
        os << vertices.at(i);
    }
    os << "})";
    return os;
}

// statics
static vect get_axis(const vertex &v1, const vertex &v2) {
    return normalize({(v2.x - v1.x), (v2.y - v1.y)});
}
static vect get_normal(const vertex &v1, const vertex &v2) {
    return normalize({-(v2.y - v1.y), (v2.x - v1.x)});
}
static float get_dot(const vect &vec, const vertex &vert) {
    return (vert.x * vec.x + vert.y * vec.y);
}
static vect normalize(const vect &vec) {
    float x = vec.x;
    float y = vec.y;
    float rate = 1.f / sqrt(x * x + y * y);
    x *= rate;
    y *= rate;
    return {x, y};
}